#ifndef STRINGHELPER_H
#define STRINGHELPER_H

#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>

class StringHelper
{
public:
	StringHelper();
	~StringHelper();

	static bool validateString(std::string str, std::string regex);
	static void trim(std::string str);
	
};

#endif // STRINGHELPER_H
