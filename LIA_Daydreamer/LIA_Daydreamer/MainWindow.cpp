#include "MainWindow.h"
#include "HomeView.h"

MainWindow::MainWindow()
{
	stack_view = new QStackedWidget(this);
	stack_view->setGeometry(0, 0, 1200, 680);

	stack_view->addWidget(new HomeView(this) );
	
	resize(1220, 680);
}

MainWindow::~MainWindow()
{

}

void MainWindow::executeAction(Action* action)
{
	action->execute(this);
}
