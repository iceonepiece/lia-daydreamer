#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QStackedWidget>
#include <vector>
#include "Patient.h"
#include "Action.h"

class MainWindow : public QWidget
{
	Q_OBJECT

public:
	MainWindow();
	~MainWindow();

	void executeAction(Action* action);

private:
	QStackedWidget* stack_view;
	std::vector<Patient*> list_patient;

};

#endif // MAINWINDOW_H
