#include "NewPatientAction.h"
#include <qdebug.h>

NewPatientAction::NewPatientAction( Message msg ):
	message(msg)
{
}

NewPatientAction::~NewPatientAction()
{

}

void NewPatientAction::execute(MainWindow* window)
{
	std::string patientName = message["patientName"];
	std::string sex = message["sex"];
	std::string fileName = message["fileName"];
	std::string location = message["location"];

	qDebug() << "Patient Name: " << patientName.c_str();
	qDebug() << "Sex: " << sex.c_str();
	qDebug() << "File Name: " << fileName.c_str();
	qDebug() << "Location: " << location.c_str();

	/*Patient* newPatient = new Patient(name, path);

	window->addPatient(newPatient);*/
}
