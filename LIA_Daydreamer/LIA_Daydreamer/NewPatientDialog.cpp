#include "NewPatientDialog.h"
#include <qDebug>

NewPatientDialog::NewPatientDialog( MainWindow* window ):
	Dialog(window)
{
	this->makeUi();
	this->makeConnection();
}

NewPatientDialog::~NewPatientDialog()
{

}

void NewPatientDialog::makeUi()
{
	this->resize(490, 245);
	this->setWindowTitle("New Patient");

	lEdit_patientName = new QLineEdit(this);
	lEdit_patientName->setGeometry(100, 20, 370, 30);

	lEdit_fileName = new QLineEdit(this);
	lEdit_fileName->setGeometry(100, 100, 370, 30);

	lEdit_location = new QLineEdit(this);
	lEdit_location->setEnabled(false);
	lEdit_location->setGeometry(100, 150, 280, 30);

	lbl_patientName = new QLabel(this);
	lbl_patientName->setText("Patient Name:");
	lbl_patientName->setGeometry(10, 20, 80, 30);

	lbl_sex = new QLabel(this);
	lbl_sex->setText("Sex:");
	lbl_sex->setGeometry(60, 60, 40, 30);

	lbl_fileName = new QLabel(this);
	lbl_fileName->setText("File Name:");
	lbl_fileName->setGeometry(30, 100, 70, 30);

	lbl_location = new QLabel(this);
	lbl_location->setText("Location:");
	lbl_location->setGeometry(40, 150, 60, 30);

	btn_ok = new QPushButton(this);
	btn_ok->setText("OK");
	btn_ok->setGeometry(300, 200, 80, 30);

	btn_cancel = new QPushButton(this);
	btn_cancel->setText("Cancel");
	btn_cancel->setGeometry(390, 200, 80, 30);

	btn_browse = new QPushButton(this);
	btn_browse->setText("Browse");
	btn_browse->setGeometry(390, 150, 80, 30);

	radBtn_male = new QRadioButton(this);
	radBtn_male->setText("Male");
	radBtn_male->setGeometry(110, 60, 60, 30);

	radBtn_female = new QRadioButton(this);
	radBtn_female->setText("Female");
	radBtn_female->setGeometry(190, 60, 60, 30);

	btnGrp_sex = new QButtonGroup(this);
	btnGrp_sex->addButton(radBtn_male);
	btnGrp_sex->addButton(radBtn_female);
}

void NewPatientDialog::makeConnection()
{
	connect(btn_ok, SIGNAL(clicked()), this, SLOT(doNewPatient()));
	connect(lEdit_patientName, SIGNAL(textChanged(QString)), this, SLOT(check()));
	
}

void NewPatientDialog::check()
{
	if (lEdit_patientName->text().size() > 0)
		btn_ok->setEnabled(true);
}

void NewPatientDialog::doNewPatient()
{
	std::string regex = "[a-zA-Z0-9\\s_-]+";
	
	std::string patientName = lEdit_patientName->text().toStdString();
	StringHelper::trim(patientName);

	std::string fileName = lEdit_fileName->text().toStdString();
	StringHelper::trim(fileName);

	std::string location = lEdit_location->text().toStdString();

	std::string errorMessage = "Errors list";

	// validate patient name
	if (patientName.size() == 0)
		errorMessage += "\n- Patient Name can't be blank!";
	else if (!StringHelper::validateString(patientName, regex))
		errorMessage += "\n- Patient Name is invalid!";

	// validate sex
	if ( btnGrp_sex->checkedId() == -1 )
		errorMessage += "\n- Sex can't be blank!";

	// validate file name
	if (fileName.size() == 0)
		errorMessage += "\n- File Name can't be blank!";
	else if (!StringHelper::validateString(fileName, regex))
		errorMessage += "\n- File Name is invalid!";

	// validate location
	if (location.size() == 0)
		errorMessage += "\n- Location can't be blank!";


	if (errorMessage.size() > 11)
	{
		QMessageBox msgBox;
		msgBox.setText(errorMessage.c_str());
		msgBox.exec();
	}

	//Message message;
	//message["patientName"] = lEdit_patientName->text().toStdString();
	//message["sex"] = std::to_string(btnGrp_sex->checkedId());
	//message["fileName"] = lEdit_fileName->text().toStdString();
	//message["location"] = lEdit_location->text().toStdString();

	//mainWindow->executeAction(new NewPatientAction(message));

}



void NewPatientDialog::init()
{
	lEdit_patientName->clear();
	lEdit_fileName->clear();
	lEdit_location->clear();
	
	radBtn_male->setChecked(false);
	radBtn_female->setChecked(false);

	btn_ok->setEnabled(false);

	this->setModal(true);
	this->show();
	
}