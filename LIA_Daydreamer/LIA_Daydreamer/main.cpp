#include <QtWidgets/QApplication>
#include "MainWindow.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include <string>
#include <qdebug.h>
#include "StringHelper.h"

void trim(std::string& in)
{
	int size = in.size();
	int firstIndex, lastIndex;

	for (int i = 0; i < size; i++)
	{
		if (in[i] == ' ')
			continue;
		else
		{
			firstIndex = i;
			break;
		}
	}

	for (int i = size - 1; i >= 0; i--)
	{
		if (in[i] == ' ')
			continue;
		else
		{
			lastIndex = i;
			break;
		}
	}
	
	in = in.substr(firstIndex, lastIndex + 1 - firstIndex );
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	MainWindow window;
	window.show();
	

	

	//std::string ice[] = { "ice",
	//	"no-hyphen",
	//	"___test_underscore",
	//	"i have a blank space",
	//	"034432",
	//	"",
	//	"_mixed-012ABC-  ",
	//	"alienWordLike@",
	//	" # ",
	//	"*&^"};

	//std::string reg = "[a-zA-Z0-9\\s_-]+";

	//for (int i = 0; i < 10; i++)
	//{
	//	if (StringHelper::validateString(ice[i], reg))
	//		qDebug() << ice[i].c_str() << "YES";
	//	else
	//		qDebug() << ice[i].c_str() << "NO";
	//}

	
	
	





	return a.exec();
}
