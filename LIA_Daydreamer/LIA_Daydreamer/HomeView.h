#ifndef HOMEVIEW_H
#define HOMEVIEW_H

#include <QPushButton>
#include "View.h"
#include "NewPatientAction.h"
#include "NewPatientDialog.h"

class HomeView : public View
{
	Q_OBJECT

public:
	HomeView( MainWindow* window );
	~HomeView();

private:
	QPushButton* btn_newPatient;
	NewPatientDialog* dlg_newPatient;
	
};

#endif // HOMEVIEW_H
