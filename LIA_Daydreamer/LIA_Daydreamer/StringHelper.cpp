#include "StringHelper.h"

StringHelper::StringHelper()
{

}

StringHelper::~StringHelper()
{

}

bool StringHelper::validateString(std::string str, std::string reg )
{
	boost::regex e(reg);
	return boost::regex_match(str, e);
}

void StringHelper::trim(std::string str)
{
	boost::trim(str);
}