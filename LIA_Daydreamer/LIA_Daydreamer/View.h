#ifndef VIEW_H
#define VIEW_H

#include <QWidget>
#include "MainWindow.h"

class View : public QWidget
{
	Q_OBJECT

public:
	View( MainWindow* window );
	~View();


protected:
	MainWindow* mainWindow;
	
};

#endif // VIEW_H
