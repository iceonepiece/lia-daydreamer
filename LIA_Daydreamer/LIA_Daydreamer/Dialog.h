#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "MainWindow.h"

class Dialog : public QDialog
{
	Q_OBJECT

public:
	Dialog( MainWindow* window );
	~Dialog();

public slots:
	virtual void init() = 0;

protected:
	virtual void makeUi() = 0;
	virtual void makeConnection() = 0;

	MainWindow* mainWindow;
	
};

#endif // DIALOG_H
