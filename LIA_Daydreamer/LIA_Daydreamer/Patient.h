#ifndef PATIENT_H
#define PATIENT_H

#include <string>
#include <vector>
#include "Folder.h"

class Patient
{
public:
	Patient();
	~Patient();

	std::string fileName;
	std::string patientName;
	std::string location;
	int sex;

	std::vector<Folder*> list_folder;

};

#endif // PATIENT_H
