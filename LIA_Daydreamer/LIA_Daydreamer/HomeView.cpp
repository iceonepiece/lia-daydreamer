#include "HomeView.h"

HomeView::HomeView( MainWindow* window ) : 
	View( window )
{
	btn_newPatient = new QPushButton(this);
	btn_newPatient->setText("New Patient");
	btn_newPatient->setGeometry(100, 100, 100, 30);

	dlg_newPatient = new NewPatientDialog(mainWindow);
	
	connect(btn_newPatient, SIGNAL(clicked()), dlg_newPatient, SLOT(init()));

}

HomeView::~HomeView()
{

}

